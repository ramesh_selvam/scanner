import UIKit
import AVFoundation
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

//    var employee_name: String?
//    var status :  String?
//    var employee_id : String?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
//        self.makeAPI(accessToken: "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEwNDIiLCJleHAiOjE2NDAyNjQzNjcsImlzcyI6InZpYWZyb250LmF6dXJld2Vic2l0ZXMubmV0IiwiYXVkIjoidmlhZnJvbnQuYXp1cmV3ZWJzaXRlcy5uZXQifQ.ocImA_nGCzgEcMEqp8S6yfvAZOXd36uTUMwomquPbdQ", checktype: "checkin")
        return true

    }

  /*
    func makeAPI(accessToken: String, checktype: String){
        
        let params = ["type": checktype] as Dictionary<String, String>

        var request = URLRequest(url: URL(string: "https://viafoneerp.azurewebsites.net/api/EmployeeNew/checkLogToken")!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue( "Bearer \(accessToken)", forHTTPHeaderField: "Authorization")


        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print(response!)
            do {
                let responseData = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>

                if (responseData["status"] as! String == "success") {
                    if (checktype == "checkin") {
                        self.employee_name = responseData["employee_name"] as? String;
                        self.employee_id = responseData["employee_id"] as? String;
                        self.status = responseData["status"] as? String;

//                        playSoundCheckin();
                    } else {
//                        playSoundCheckout();
                    }


                } else {
                    self.playSound()
                    //console.log('failure');

                }

            } catch {
                print("error")
            }
        })

        task.resume()

        
    }*/
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    var player: AVAudioPlayer?

    func playSound() {
        guard let url = Bundle.main.url(forResource: "checkin", withExtension: "mpeg") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            /* iOS 10 and earlier require the following line:
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */

            guard let player = player else { return }

            player.play()

        } catch let error {
            print(error.localizedDescription)
        }
    }


}

