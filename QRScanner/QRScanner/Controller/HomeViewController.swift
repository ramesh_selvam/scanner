//
//  HomeViewController.swift
//  QRScanner
//
//  Created by RK on 26/10/21.
//  Copyright © 2021 KM, Abhilash. All rights reserved.
//

import UIKit

import AVFoundation
class HomeViewController: UIViewController {
    
    var player: AVAudioPlayer?
    var timer: Timer?

    @IBOutlet weak var successImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var scanLabel: UILabel!

    @IBOutlet weak var logoImageView: UIImageView!
    
    let progressHUD = ProgressHUD(text: "Loading")


    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(progressHUD)
        
        progressHUD.hide()
//                self.makeAPI(accessToken: "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEwNDIiLCJleHAiOjE2NDAzMzAwNTUsImlzcyI6InZpYWZyb250LmF6dXJld2Vic2l0ZXMubmV0IiwiYXVkIjoidmlhZnJvbnQuYXp1cmV3ZWJzaXRlcy5uZXQifQ.bhBd-lPtKeAg2qBD8nUhoHJcOHrZImr2anFl1GxQv7g", checktype: "checkout")

        // Do any additional setup after loading the view.
    }
    
    func startTimer(){
        
        if let _ = timer{
            timer?.invalidate()
            timer = nil
        }
        
        timer = Timer.scheduledTimer(timeInterval: 15, target: self, selector: #selector(self.resetUI), userInfo: nil, repeats: false)

    }

    @objc func resetUI(){
        if let _ = timer{
            timer?.invalidate()
            timer = nil
        }
        
        self.nameLabel.isHidden = true
        self.idLabel.isHidden = true
        self.scanLabel.isHidden = true
        self.successImage.isHidden = true
        self.logoImageView.isHidden = false


    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc: QRScannerViewController =   segue.destination as? QRScannerViewController{
            vc.delegate = self
        }
        
    }
    
    
    func playSound(soundFileName: String) {
        guard let url = Bundle.main.url(forResource: soundFileName, withExtension: "mpeg") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = player else { return }
            
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    


}

//MARK: - QRScannerViewControllerDelegate
extension HomeViewController: QRScannerViewControllerDelegate{
    
    func didScanedWithData(responseData:Dictionary<String, Any>, checktype: String){
        
        DispatchQueue.main.async {
            
            self.nameLabel.isHidden = false
            self.idLabel.isHidden = false
            self.scanLabel.isHidden = false
            self.successImage.isHidden = false
            self.logoImageView.isHidden = true

            self.nameLabel.text = responseData["employee_name"] as? String;
            self.idLabel.text = responseData["employee_id"] as? String;
           
            if (responseData["status"] as! String == "success") {

            if (checktype == "checkin") {
                self.playSound(soundFileName: "checkin")
                self.scanLabel.text = "CHECKA IN";
                
            } else {
                self.scanLabel.text = "CHECKA OUT";
                self.playSound(soundFileName: "checkOut")
            }
                self.successImage.image = UIImage(named: "success.png")
            } else {
                self.successImage.image = UIImage(named: "fail.png")
                let errorMessage = responseData["message"] as? String ?? ""
                self.scanLabel.text = errorMessage;
//                self.showToast(message: errorMessage)

            }
            self.startTimer()
        }

        
    }

    func didDismisWith(ScanDatastring: String?) {
        
        guard let dataString = ScanDatastring else{
            self.logoImageView.isHidden = false
            self.nameLabel.isHidden = true
            self.idLabel.isHidden = true
            self.scanLabel.isHidden = true
            self.successImage.isHidden = true

            return
        }
        
        if let dic = self.convertStringToDictionary(text: dataString), let ctoken = dic["ctoken"] as? String, let checktype = dic["type"] as? String{
            self.makeAPI(accessToken: ctoken, checktype: checktype)
        }else{
            self.showToast(message: "Invalid Data")
        }
        

    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }

    
    func makeAPI(accessToken: String, checktype: String){
        progressHUD.show()
        let params = ["type": checktype] as Dictionary<String, String>
        
        var request = URLRequest(url: URL(string: "https://viafoneerp.azurewebsites.net/api/EmployeeNew/checkLogToken")!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue( "Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print(response!)
            DispatchQueue.main.async {
                self.progressHUD.hide()
            }
            do {
                let responseData = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                    print(responseData)
                self.didScanedWithData(responseData: responseData, checktype: checktype)
                
            } catch {

                print("error")
            }
            
        })
        
        task.resume()
        
    }
    

}


class ProgressHUD: UIVisualEffectView {

  var text: String? {
    didSet {
      label.text = text
    }
  }

    let activityIndictor: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
  let label: UILabel = UILabel()
  let blurEffect = UIBlurEffect(style: .light)
  let vibrancyView: UIVisualEffectView

  init(text: String) {
    self.text = text
    self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
    super.init(effect: blurEffect)
    self.setup()
  }

  required init?(coder aDecoder: NSCoder) {
    self.text = ""
    self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
    super.init(coder: aDecoder)
    self.setup()
  }

  func setup() {
    contentView.addSubview(vibrancyView)
    contentView.addSubview(activityIndictor)
    contentView.addSubview(label)
    activityIndictor.startAnimating()
  }

  override func didMoveToSuperview() {
    super.didMoveToSuperview()

    if let superview = self.superview {

      let width = superview.frame.size.width / 2.3
      let height: CGFloat = 50.0
      self.frame = CGRect(x: superview.frame.size.width / 2 - width / 2,
                      y: superview.frame.height / 2 - height / 2,
                      width: width,
                      height: height)
      vibrancyView.frame = self.bounds

      let activityIndicatorSize: CGFloat = 40
      activityIndictor.frame = CGRect(x: 5,
                                      y: height / 2 - activityIndicatorSize / 2,
                                      width: activityIndicatorSize,
                                      height: activityIndicatorSize)

      layer.cornerRadius = 8.0
      layer.masksToBounds = true
      label.text = text
      label.textAlignment = NSTextAlignment.center
      label.frame = CGRect(x: activityIndicatorSize + 5,
                           y: 0,
                           width: width - activityIndicatorSize - 15,
                           height: height)
      label.textColor = UIColor.gray
      label.font = UIFont.boldSystemFont(ofSize: 16)
    }
  }

  func show() {
    self.isHidden = false
  }

  func hide() {
    self.isHidden = true
  }
}
