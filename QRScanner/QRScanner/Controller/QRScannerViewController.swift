
import UIKit
import AVFoundation

protocol QRScannerViewControllerDelegate{
    func didDismisWith(ScanDatastring: String?)
}

class QRScannerViewController: UIViewController {
    
    var timer: Timer?
    var delegate: QRScannerViewControllerDelegate?
    
    @IBOutlet weak var scannerView: QRScannerView! {
        didSet {
            scannerView.delegate = self
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.configureVideoOrientation()
    }

    private func configureVideoOrientation() {
         let previewLayer = self.scannerView.layer
           if let connection = previewLayer.connection {
            let orientation = UIDevice.current.orientation

            if connection.isVideoOrientationSupported,
                let videoOrientation = AVCaptureVideoOrientation(rawValue: orientation.rawValue) {
                previewLayer.frame = self.view.bounds
                connection.videoOrientation = videoOrientation
            }
        }
    }

    
    var qrData: QRData? = nil {
        didSet {
            if qrData != nil {
                self.performSegue(withIdentifier: "detailSeuge", sender: self)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let _ = timer{
            timer?.invalidate()
            timer = nil
        }
        
        timer = Timer.scheduledTimer(timeInterval: 15, target: self, selector: #selector(self.closeScannerView1), userInfo: nil, repeats: false)
        if !scannerView.isRunning {
            scannerView.startScanning()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let _ = timer{
            timer?.invalidate()
            timer = nil
        }
        if scannerView.isRunning {
            scannerView.stopScanning()
        }
    }
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
//        closeScannerView(scanDataString: "{\"ctoken\":\"eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEwNDIiLCJleHAiOjE2NDAzMzAxNDMsImlzcyI6InZpYWZyb250LmF6dXJld2Vic2l0ZXMubmV0IiwiYXVkIjoidmlhZnJvbnQuYXp1cmV3ZWJzaXRlcy5uZXQifQ.Ewb3mtLn7BszJWP0ufQDdRwhdqfVDbuT4BVCh3_aU5w\",\"type\":\"checkin\"}")
        closeScannerView()
    }
    
    @objc func closeScannerView1(){
        closeScannerView()
    }

     func closeScannerView(scanDataString: String? = nil){
        
        DispatchQueue.main.async { [self] in
            if let _ = timer{
                timer?.invalidate()
                timer = nil
            }
            self.delegate?.didDismisWith(ScanDatastring: scanDataString)

            self.dismiss(animated: true) {
                if  self.scannerView.isRunning {
                    self.scannerView.stopScanning()
                }

                
            }
        }
        
    }
}
extension QRScannerViewController: QRScannerViewDelegate {
    func qrScanningDidStop() {
        //        let buttonTitle = scannerView.isRunning ? "STOP" : "SCAN"
        //        scanButton.setTitle(buttonTitle, for: .normal)
    }
    
    func qrScanningDidFail() {
        presentAlert(withTitle: "Error", message: "Scanning Failed. Please try again")
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        self.closeScannerView(scanDataString: str)
    }
    
    
}


extension QRScannerViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailSeuge", let viewController = segue.destination as? DetailViewController {
            viewController.qrData = self.qrData
        }
    }
}

